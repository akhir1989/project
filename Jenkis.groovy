 pipeline {
    agent {
      label 'WORKSTATION'
    }
    tools {
       maven 'maven-3.8.4'
       }

	stages {
	
     stage('Compile the Code') {
       steps {
        sh 'mvn package'
      }
    }

      stage('Check the Code Quality') {
        steps {
            sh ' sonar-scanner -X -Dsonar.projectKey=iwayq -Dsonar.sources=. -Dsonar.host.url=http://34.125.104.3:9000 -Dsonar.login=admin -Dsonar.password=admin123'
        }
      }

      stage('Test Cases') {
        steps {
          sh 'echo Test Cases'
        }
      }
       stage('Publish Artifacts') {
        steps {
          script {
            //common.publishArtifacts()
            println 'Publish Artifacts'
            }
          }  
       }   
   }
 }